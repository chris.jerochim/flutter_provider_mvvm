import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppTheme {
  AppTheme._();

  //*****************************************
  // Spacing
  //*****************************************

  static const double space0 = 2.0;
  static const double space1 = 4.0;
  static const double space2 = 2.0;
  static const double space3 = 8.0;
  static const double space4 = 12.0;
  static const double space5 = 16.0;
  static const double space6 = 18.0;
  static const double space7 = 24.0;
  static const double space8 = 32.0;
  static const double space9 = 40.0;
  static const double space10 = 48.0;
  static const double space11 = 64.0;

  //*****************************************
  // Images
  //*****************************************

  // Example referencing file names from the asset folders, folder location needs to be referenced within the pubspec.yaml
  // static const String imageTexture1 = 'assets/images/texture_1.png';
  // static const String imageTexture2 = 'assets/images/texture_2.png';
  // static const String imageTexture3 = 'assets/images/texture_3.png';
  // static const String imageTexture4 = 'assets/images/texture_4.png';

  //*****************************************
  // Colours
  //*****************************************

  static const Color colorBrandPrimary = Color(0xFFF5C577);
  static const Color colorBrandSecondary = Color(0xFF1AA294);
  static const Color colorBrandTertiary = Color(0xFF2A88AA);
  static const Color colorBrandQuaternary = Color(0xFFF07A5A);

  static const Color colorRamp0 = Color(0xFF000000);
  static const Color colorRamp1 = Color(0xFF2C1A32);
  static const Color colorRamp2 = Color(0xFFB4AEB8);
  static const Color colorRamp3 = Color(0xFFD1CED4);
  static const Color colorRamp4 = Color(0xFFEEEEF1);
  static const Color colorRamp9 = Color(0xFFFFFFFF);

  //*****************************************
  // Typography - Designs should  align to the following naming convention:
  // https://api.flutter.dev/flutter/material/TextTheme-class.html
  //*****************************************
  static final TextTheme _textTheme = TextTheme(
    headline1: GoogleFonts.poppins(
      fontSize: 30,
      height: 1.1,
      color: AppTheme.colorRamp1,
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.bold,
    ),
    headline2: GoogleFonts.poppins(
      fontSize: 24,
      height: 1.3,
      color: AppTheme.colorRamp1,
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.bold,
    ),
    headline3: GoogleFonts.poppins(
      fontSize: 14,
      color: AppTheme.colorRamp1,
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.bold,
    ),
    headline4: GoogleFonts.poppins(
      fontSize: 14,
      color: AppTheme.colorRamp1,
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.bold,
    ),
    subtitle1: GoogleFonts.roboto(
      fontSize: 16,
      color: AppTheme.colorRamp1,
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.normal,
    ),
    subtitle2: GoogleFonts.roboto(
      fontSize: 14,
      color: AppTheme.colorRamp1,
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.normal,
    ),
    bodyText1: GoogleFonts.roboto(
      color: AppTheme.colorRamp1,
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.normal,
      fontSize: 16,
    ),
    bodyText2: GoogleFonts.roboto(
      height: 1.4,
      color: AppTheme.colorRamp1,
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.normal,
      fontSize: 14,
    ),
    button: GoogleFonts.poppins(
      color: AppTheme.colorRamp9,
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.bold,
      fontSize: 18,
    ),
    caption: GoogleFonts.robotoMono(
      color: AppTheme.colorRamp1,
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.normal,
      fontSize: 18,
    ),
    overline: GoogleFonts.poppins(
      fontSize: 14,
      color: AppTheme.colorRamp1,
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.normal,
    ),
  );

  //*****************************************
  // Public accessor
  //*****************************************
  static final ThemeData theme = ThemeData(
    visualDensity: VisualDensity.adaptivePlatformDensity,
    textTheme: _textTheme,
  );
}
