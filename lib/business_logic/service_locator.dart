import 'package:flutter_provider_mvvm/business_logic/services/authentication/authentication.dart';
import 'package:flutter_provider_mvvm/business_logic/services/navigation/navigation.dart';
import 'package:flutter_provider_mvvm/business_logic/view_models/home_screen_viewmodel.dart';
import 'package:flutter_provider_mvvm/business_logic/view_models/splash_screen_viewmodel.dart';
import 'package:flutter_provider_mvvm/environment_config.dart';
import 'package:get_it/get_it.dart';

GetIt serviceLocator = GetIt.instance;

void setupServiceLocator([bool isTest = false]) {
  //*********************************
  // Services
  //*********************************

  serviceLocator
      .registerLazySingleton<NavigationService>(() => NavigationServiceImpl());

  isTest
      ? serviceLocator.registerLazySingleton<AuthenticationService>(
          () => AuthenticationServiceFake())
      : serviceLocator.registerLazySingleton<AuthenticationService>(
          () => AuthenticationServiceImpl(
                domain: EnvironmentConfig.AUTH_DOMAIN,
                clientId: EnvironmentConfig.AUTH_CLIENT_ID,
                redirectUri: EnvironmentConfig.AUTH_REDIRECT_URI,
              ));

  //*********************************
  // View Models
  //*********************************

  serviceLocator
      .registerFactory<SplashScreenViewModel>(() => SplashScreenViewModel());

  serviceLocator
      .registerFactory<HomeScreenViewModel>(() => HomeScreenViewModel());
}
