import 'package:flutter/material.dart';
import 'package:flutter_provider_mvvm/business_logic/service_locator.dart';
import 'package:flutter_provider_mvvm/business_logic/services/navigation/navigation.dart';
import 'package:flutter_provider_mvvm/presentation/screens/home/home_screen.dart';

class SplashScreenViewModel extends ChangeNotifier {
  //******************************************
  // Services
  //******************************************

  final NavigationService _navigationService =
      serviceLocator<NavigationService>();

  //******************************************
  // Public
  //******************************************

  void goToHomeScreen() async {
    await _navigationService.navigateTo(HomeScreen.routeName);
  }
}
