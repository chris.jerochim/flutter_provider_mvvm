import 'package:flutter/material.dart';
import 'package:flutter_provider_mvvm/business_logic/service_locator.dart';
import 'package:flutter_provider_mvvm/business_logic/services/navigation/navigation.dart';
import 'package:flutter_provider_mvvm/presentation/screens/screens.dart';

class HomeScreenViewModel extends ChangeNotifier {
  //******************************************
  // Vars
  //******************************************

  //******************************************
  // Services
  //******************************************

  final NavigationService _navigationService =
      serviceLocator<NavigationService>();

  //******************************************
  // Public
  //******************************************

  void back() {
    _navigationService.navigateReplaceTo(SplashScreen.routeName);
  }
}
