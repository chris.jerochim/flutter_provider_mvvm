abstract class AuthenticationService {
  Future<void> login([String language = 'en']);
  String getAccessToken();
  Future<bool> isAuthenticated();
  Future<void> logout();
}
