import 'package:flutter_provider_mvvm/business_logic/services/authentication/authentication.dart';

class AuthenticationServiceFake implements AuthenticationService {
  bool _isLoggedIn = false;

  @override
  Future<void> login([String language = 'en']) async {
    _isLoggedIn = true;
  }

  @override
  String getAccessToken() {
    return '';
  }

  @override
  Future<bool> isAuthenticated() async {
    return _isLoggedIn;
  }

  @override
  Future<void> logout() async {
    _isLoggedIn = false;
    return null;
  }
}
