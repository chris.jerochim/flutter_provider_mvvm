import 'package:flutter_provider_mvvm/business_logic/services/authentication/authentication.dart';
import 'package:http/http.dart' as http;

import 'package:flutter_appauth/flutter_appauth.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

const _kRefreshKey = 'refresh_token';

// Internal type for the service to abstract the response for both refreshing a token vs login
class AuthResponse {
  final String accessToken;
  final String refreshToken;

  AuthResponse({this.accessToken, this.refreshToken});
}

class AuthenticationServiceImpl implements AuthenticationService {
  final String domain;
  final String clientId;
  final String redirectUri;

  AuthenticationServiceImpl({
    this.domain,
    this.clientId,
    this.redirectUri,
  });

  //***********************************
  // vars
  //***********************************

  final FlutterAppAuth _appAuth = FlutterAppAuth();
  final FlutterSecureStorage _secureStorage = const FlutterSecureStorage();

  // Retain in app memory do not store within app
  String _accessToken;

  // Thoughts: Validate the need for isAuthenticated?
  // Prefer to always check against the server if the access token is valid e.g. response of 401
  // This will ensure the access/refresh tokens always align to server configuration is 1:1 with the app, instead of referring the the token expiry locally.

  // Situations to infer authentication
  // When user launches app, this will always trigger login function, this will either refresh the token or prompt the user to re-login
  // When API calls are made this would be a good indication for the user to re-login again.

  //***********************************
  // Services
  //***********************************

  //***********************************
  // Private
  //***********************************

  // Method to refresh token and ensure user is authenticated
  Future<TokenResponse> _refreshAuthentication(String refreshToken) {
    if (refreshToken == null) return null;
    final request = TokenRequest(
      clientId,
      redirectUri,
      issuer: 'https://$domain',
      refreshToken: refreshToken,
      scopes: ['openid', 'offline_access'],
    );
    return _appAuth.token(request);
  }

  Future<AuthorizationTokenResponse> _login(String language) {
    final request = AuthorizationTokenRequest(
      clientId, redirectUri,
      issuer: 'https://$domain', scopes: ['openid', 'offline_access'],
      // Always prompt the user to login
      promptValues: ['login'], additionalParameters: {'language': language},
    );

    return _appAuth.authorizeAndExchangeCode(request);
  }

  // TODO - Consider moving the request management to a separate service when there's more of a need
  Future _logoutFromServer(String accessToken) async {
    final path = 'https://$domain/v2/logout';
    try {
      await http.get(path, headers: {'Authorization': 'Bearer $accessToken'});
    } catch (e) {
      rethrow;
    }
  }

  // TODO - Consider splitting this out if there are more needs for secure storage
  Future<void> _storeRefreshToken(String refreshToken) =>
      _secureStorage.write(key: _kRefreshKey, value: refreshToken);

  Future<void> _destroyRefreshToken() =>
      _secureStorage.delete(key: _kRefreshKey);

  Future<String> _retrieveRefreshToken() =>
      _secureStorage.read(key: _kRefreshKey);

  //***********************************
  // Public
  //***********************************

  Future<AuthResponse> _refreshUser() async {
    try {
      // Check for token
      final refreshToken = await _retrieveRefreshToken();
      if (refreshToken == null) return null;

      // Refresh user
      final response = await _refreshAuthentication(refreshToken);
      return AuthResponse(
        accessToken: response.accessToken,
        refreshToken: response.refreshToken,
      );
    } catch (e) {
      rethrow;
    }
  }

  Future<AuthResponse> _loginUser(String language) async {
    try {
      final response = await _login(language);
      return AuthResponse(
        accessToken: response.accessToken,
        refreshToken: response.refreshToken,
      );
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<void> login([String language = 'en']) async {
    try {
      // 1. Check if user has already authenticated
      final response = await _refreshUser();
      // 2. Login if response is null
      final authResponse = response ?? await _loginUser(language);
      // 3. Store refresh token
      await _storeRefreshToken(authResponse.refreshToken);
      // Store refresh token and add access token to memory
      _accessToken = authResponse.accessToken;
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<bool> isAuthenticated() async {
    try {
      // Check current refresh token against auth0
      final response = await _refreshUser();
      // If fails assume user is not authenticated
      if (response == null) return false;
      // Store latest refresh token into secure storage
      await _storeRefreshToken(response.refreshToken);
      // Persist access token in memory
      _accessToken = response.accessToken;
      return true;
    } catch (e) {
      // If invalid token or something went wrong, track it and destroy token in storage
      // Destroy secure token in memory
      await _destroyRefreshToken();
      // User is not authenticated
      return false;
    }
  }

  @override
  String getAccessToken() {
    return _accessToken;
  }

  @override
  Future<void> logout() async {
    try {
      // 1. Ensure logout from server is successful
      await _logoutFromServer(_accessToken);
      // 2. Remove refresh token from secure storage
      await _destroyRefreshToken();
    } catch (e) {
      rethrow;
    }
  }
}
