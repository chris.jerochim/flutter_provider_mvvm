import 'package:flutter/material.dart';
import 'package:flutter_provider_mvvm/business_logic/services/navigation/navigation.dart';

class NavigationServiceImpl implements NavigationService {
  //*********************************
  // Vars
  //*********************************
  final GlobalKey<NavigatorState> _navigationKey = GlobalKey<NavigatorState>();

  //*********************************
  // Public
  //*********************************
  @override
  GlobalKey<NavigatorState> get navigationKey => _navigationKey;

  @override
  Future<dynamic> navigateTo(String routeName, [dynamic args]) {
    return navigationKey.currentState.pushNamed(routeName, arguments: args);
  }

  @override
  Future<dynamic> navigateReplaceTo(String routeName, [dynamic args]) {
    return navigationKey.currentState
        .pushReplacementNamed(routeName, arguments: args);
  }

  @override
  void goBack([dynamic args]) {
    return navigationKey.currentState.pop(args);
  }

  @override
  void backToRoot(String routeName) {
    return navigationKey.currentState.popUntil(ModalRoute.withName(routeName));
  }
}
