import 'package:flutter/material.dart';

abstract class NavigationService {
  GlobalKey<NavigatorState> get navigationKey;
  Future<dynamic> navigateTo(String routeName, [dynamic args]);
  Future<dynamic> navigateReplaceTo(String routeName, [dynamic args]);
  void goBack([dynamic args]);
  void backToRoot(String routeName);
}
