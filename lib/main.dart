import 'package:flutter/material.dart';
import 'package:flutter_provider_mvvm/app.dart';
import 'package:flutter_provider_mvvm/business_logic/service_locator.dart';

void main({bool isTest = false}) {
  // Initiate Dependencies
  setupServiceLocator(isTest);

  //
  runApp(App());
}
