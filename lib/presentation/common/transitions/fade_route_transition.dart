import 'package:flutter/material.dart';

class FadeRouteTransition extends PageRouteBuilder {
  final Widget page;

  FadeRouteTransition({this.page, RouteSettings settings})
      : super(
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) =>
              page,
          transitionsBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child,
          ) =>
              FadeTransition(
            opacity:
                Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
              parent: animation,
              curve: Curves.easeInOutQuad,
            )),
            child: child,
          ),
          settings: settings,
        );
}
