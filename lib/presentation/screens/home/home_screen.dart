import 'package:flutter/material.dart';
import 'package:flutter_provider_mvvm/business_logic/service_locator.dart';
import 'package:flutter_provider_mvvm/business_logic/view_models/home_screen_viewmodel.dart';
import 'package:flutter_provider_mvvm/theme.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  static const String routeName = '/home';
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final HomeScreenViewModel model = serviceLocator<HomeScreenViewModel>();

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<HomeScreenViewModel>(
      create: (_) => model,
      child: Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Home screen', style: Theme.of(context).textTheme.headline1),
              SizedBox(height: AppTheme.space4),
              Text('Welcome ', style: Theme.of(context).textTheme.headline4),
              SizedBox(height: AppTheme.space4),
              RaisedButton(
                child: Text('Back'),
                onPressed: model.back,
              )
            ],
          ),
        ),
      ),
    );
  }
}
