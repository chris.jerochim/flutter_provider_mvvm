import 'package:flutter/material.dart';
import 'package:flutter_provider_mvvm/business_logic/service_locator.dart';
import 'package:flutter_provider_mvvm/business_logic/view_models/splash_screen_viewmodel.dart';
import 'package:flutter_provider_mvvm/theme.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {
  static const String routeName = '/splash';

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final SplashScreenViewModel model = serviceLocator<SplashScreenViewModel>();

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<SplashScreenViewModel>(
      create: (_) => model,
      child: Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Splash screen',
                  style: Theme.of(context).textTheme.headline1),
              SizedBox(height: AppTheme.space4),
              RaisedButton(
                child: Text('Continue'),
                onPressed: model.goToHomeScreen,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
