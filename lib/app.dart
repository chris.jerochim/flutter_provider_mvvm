import 'package:flutter/material.dart';
import 'package:flutter_provider_mvvm/business_logic/service_locator.dart';
import 'package:flutter_provider_mvvm/business_logic/services/services.dart';
import 'package:flutter_provider_mvvm/presentation/screens/screens.dart';
import 'package:flutter_provider_mvvm/theme.dart';

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Boilerplate app',
      theme: AppTheme.theme,
      navigatorKey: serviceLocator<NavigationService>().navigationKey,
      initialRoute: SplashScreen.routeName,
      routes: {
        SplashScreen.routeName: (_) => SplashScreen(),
        HomeScreen.routeName: (_) => HomeScreen(),
      },
    );
  }
}
