# Flutter Provider/MVVM Pattern  (Boilerplate WIP)

View primer of Flutter and the PROS/CONS [here](https://confluence.bcgdv.io/display/ENG/SYD+ENG+Flutter)


---

## Core Tools & Frameworks

#### Dependencies
- **Flutter**
- **[get it](https://pub.dev/packages/get_it)** - Dependency injection
- **[provider](https://pub.dev/packages/provider)** - manage re-build of widgets based no model updates
- **[environment_config](https://pub.dev/packages/environment_config)** - Generate config file during build time - manage secrets
- **[google_fonts](https://pub.dev/packages/google_fonts)** - Load Google fonts to theme
- **[intl](https://pub.dev/packages/intl)** - Multilingual
- **[AppAuth](https://pub.dev/packages/flutter_appauth)** - Follows OpenID Connect Protocol for Authentication
- **[Flutter secure storage](https://pub.dev/packages/flutter_secure_storage)** - Store refresh token securely 
 

#### Dev Dependencies
- **[flutter_launcher_icons](https://pub.dev/packages/flutter_launcher_icons)** - Automate generation of launch icons
- **[flutter_driver](https://flutter.dev/docs/testing#integration-tests)** - End to End testing
- **[pedantic](https://pub.dev/packages/pedantic)** - Static code analysis

---

### TODO list of features to be implemented into boilerplate
- App Auth
- Intl
- Environment config
- flutter_launcher_icons
- General clean up readme file
---

### Application Architecture overview

Working with Flutter, there are many ways to manage application state. The following implementation provides consideration around the following:

- Separation/decoupling
- Minimal framework/library overhead - compared to other libraries such as BLoC
- Flexibility -
- Maintainability
- Ability to mock services which are not yet available
- Testability

The structure is influenced heavily from the "Clean Architecture" and MVVM pattern.

---

## Application structure overview

```
business_logic/
    - models/ (entities)
    - services/
    - transforms/
    - utils/
    - view_models/
        - presentation_models/
        - home_screen_viewmodel.dart
    - service_locator.dart

presentation/
    - common/
			- widgets/
    - screens/
        - Home/
            - widgets/
                - header.dart
            - home_screen.dart
        - ....
```

---

## General Rules

**Business Logic**

- **Models** (Entities) within business logic represent the data source; they are the core of the application.
- **Services** are the side effects of the application, achieve a specific task, e.g. Storage service.
- **Transforms** converts models to presentation models and presentation models to models.
- **View Models** translate the data and put it in a presentable form that the UI can use. There should be one view model per screen.
- **Presentation** **models** are models explicitly used for the UI and are not aware of (entities)

**Presentation**

- **Common** generally contains or the current widgets or elements which are directly related to UI.

- **Screens** will have a collection of folders for each screen. The folders will have the screen file and all widgets related. The majority of widgets implemented are related or tied to a specific screen.

## Dependency injection

## Services

All services are singletons, are bootstrapped when the application launches and remain active throughout the life-cycle of the application. One of many services can be injected into a view model.

## View Models

Are instance-based and are initiated when the associated screen is rendered. There should only be one view model per screen; a view model can be considered for a widget which large amounts of complexity within a screen.

## **Libraries**

[get_it](https://pub.dev/packages/get_it)

Provides the ability for dependency injection and is the most mature and standard method currently available within the Flutter echo system.

[Provider](https://pub.dev/packages/provider)

Most recently is the recommended method by Flutter to manage state within an application. The provider element enables the ability to notify the UI of state change and allows the widgets (UI) to consume. The "provider" library contains convenience widgets to support discrete widget updates which help for better performance.

### Resources: 

Reference to the above architecture
https://www.raywenderlich.com/6373413-state-management-with-provider

---

### Tools & Services

#### Build & Deployments with CodeMagic
Simplify the build process with CodeMagic all workflows can be configured via a codemagic.yaml. Codemagic  specialises in builds for Flutter outputing for both iOS and Android.
More details can be found here https://docs.codemagic.io/getting-started/creating-workflows/

#### Pre-Commit Hooks

Before any development ensure you have the latest Git software installed and run the following command:

```
git config core.hooksPath .githooks/
```

This will update git to point to the `.githooks` folder to run the executable scripts for pre-commit and pre-push

---

#### launch icon generation

Place icon in the following directory `assets/icon/`

Run the following command

```
flutter pub run flutter_launcher_icons:main
```

More details can be found here
https://github.com/fluttercommunity/flutter_launcher_icons

---

### **flutter_localizations**

Currently supports 77 languages and Bahasa is one of the supported languages.

### Flutter intl

[intl | Dart Package](https://pub.dev/packages/intl)

 This package provides internationalisation and localisation facilities, including message translation, plurals and genders, date/number formatting and parsing, and bidirectional text.

### Android Studio intl plugin

Simplifies the setup and implementation of new languages within the application. To install launch Android studio go to preferences > plugins and search the market place for "Intl"

---

## Implementation approach

There are 2 types of translations required within the application. Static translations that sit within the application and content specific translations which are served from the backend/content provider.

The focus will be purely on the static translations that occur within the application. The devices OS will inform the application as to which language to serve (User preferences)

To manage each language there is a language file generated as per below:

```yaml
l10n
	- intl_en_US.arb
	- intl_id.arb
```

Key value pairs are defined for each language file as per below:

`intl_en_us.arb`

```json
{
  "@@locale": "en_US",
  "welcome": "Welcome {userName}"
}
```

 

`intl_id.arb`

```json
{
  "@@locale": "id",
  "welcome": "Selamat datang {userName}"
}
```

To enable language translations within the Flutter application, you need to reference the application context as per below:

```dart
...
Text(S.of(context).welcome('Chris')),
```

The implementation allows the ability to add placeholders within the translation and to manage handle polarisations.

To read further on implementation and measurement conversion read the following article, provides clear detail and examples of implementation. 

 

[Internationalizing and Localizing Your Flutter App](https://www.raywenderlich.com/10794904-internationalizing-and-localizing-your-flutter-app)


## Integration tests (Details to come) 

To run integration tests ensure you have an emulator open and run the following command:

```
flutter drive  --target=test_driver/features/navigation.dart
```

## Authentication (WIP)

###NOTE
#### Current implementation

Returns an Opaque access token which the backend can verify via the `/userinfo` endpoint. There is a rate limit on calling `/userinfo` endpoint e.g. 5 requests a minute

#### Recommended
Define an API in auth0 which will generate an audience. Apply the `audience: 'http://api.audience'` to the `additionalParameters` attribute for both refresh and login functions within the auth service impl.
This will provide a JWT access token which the backend can verify via `https://YOUR_DOMAIN/.well-known/jwks.json` without the need of hitting the `/userinfo` endpoint.

Example node implementation for (reference)[https://auth0.com/docs/quickstart/backend/nodejs]


1. Need to specify the url callback for both iOS and Android e.g. com.auth0.flutterdemo://login-callback

More detail can be found (here)[https://auth0.com/blog/get-started-with-flutter-authentication/#Configure-Dependencies-and-Callback-URL];

2. Once devices have been configured, make the following changes in Auth0

NOTE - The current implementation requires the refresh token behaviour to be Rotating

https://auth0.com/blog/get-started-with-flutter-authentication/#Set-Up-Auth0

3. Specify the environment variables by running the command below




## Environment config

NOTE - Need to run this command before launching app.

To generate environment variables run the following command.

```
flutter pub run environment_config:generate -d "https://auth-domain" -i "client-id" -r "https://redirect-url"
```

